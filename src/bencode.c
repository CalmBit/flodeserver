/*  
    Copyright (C) 2015 Punkt Creative LLC

    This file is part of Flode.

    Flode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Flode.  If not, see <http://www.gnu.org/licenses/>. */

#include "bencode.h"

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

char errBuffer[BENCODE_STRING_SIZE];

char *getError()
{
  return &errBuffer[0];
}

bencodeObject *decode_bencode(char *toDecode)
{
  /* TODO: Finish up dictionary implementation
     size_t counter: counter for character data. */
  size_t counter = 0;
  /* size_t bufferCounter: counter for buffer data */
  size_t bufferCounter = 0;
  /* char buffer: buffer for data to be utilized in bencode object 
     construction */
  char buffer[BENCODE_STRING_SIZE];
  /* bencodeObject *object: pointer to new bencode object, freed on 
     error */
  bencodeObject *object = 
    (bencodeObject*)malloc(sizeof(bencodeObject));
  /* Starting from the beginning of the character stream, switch the 
     first character */
  switch(toDecode[counter])
    {
      /* If an integer */
    case 'i':
      {
	/* Loop untiil the integer end delimiter */
	while(toDecode[counter] != 'e')
	  {
	    counter++;
	    /* If the stream ends in the middle of processing, fail */
	    if(toDecode[counter] == '\0')
	      {
		strcpy(&errBuffer[0], 
		       "Bencoded string ended prematurely!");
		return NULL;
	      }
	    /* Catching edge cases thanks to the above counter 
	       incrementation */
	    else if(toDecode[counter] == 'e') break;
	    /* Load each new character into the buffer, and increment
	       the bufferCounter */
	    buffer[bufferCounter] = toDecode[counter];
	    bufferCounter++;
	  }
	/* New object is of type int, and strtol the buffer's 
	   integer data */
	object->val.integer = (int64_t)strtol(buffer,NULL,10);
	object->type = BC_INT;
	return object;
      }
      
      /* Hackety-hack for string data, thanks to the use of integers 
	 as a start-delimiter */
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '0':
      {
	size_t stringSize;
	size_t i;
	while(toDecode[counter] != ':')
	  {
	    if(toDecode[counter] == '\0')
	      {
		strcpy(&errBuffer[0], "Bencoded string ended prematurely!"
		       "Section: String Parsing");
		return NULL;
	      }
	    buffer[bufferCounter] = toDecode[counter];
	    counter++;
	    bufferCounter++;
	  }
	stringSize = (size_t) strtol(&buffer[0], NULL, 10);
	counter++;
	for(i = 0;i < stringSize;i++)
	  {
	    buffer[i] = toDecode[counter];
	    counter++;
	  }
	buffer[stringSize+1] = '\0';
	strcpy(object->val.string, &buffer[0]);
	object->type = BC_STR;
	return object;
      }
      
    case 'd':
      {
	bencodeDictEntry *entry = 
	  (bencodeDictEntry *)malloc(sizeof(bencodeDictEntry)		\
				     * 256);
	size_t size = 0;
	bencodeDict *dict = 
	  (bencodeDict *)malloc(sizeof(bencodeDict));
	bool dictKeying = true;
	while(toDecode[counter] != 'e') 
	  {
	    if(size == 0 && dictKeying) counter++;
	    if(toDecode[counter] == '\0')
	      {
		strcpy(&errBuffer[0], 
		       "Bencoded string ended prematurely!"
		       "Section: Initial Check");
		free(dict);
		free(entry);
		return NULL;
	      }
	    else if(toDecode[counter] == 'e') break;
	    if(toDecode[counter] >= '0' && toDecode[counter] <= '9')
	      {
		size_t stringSize;
		int i;
		bencodeObject *obj;
		while(toDecode[counter] != ':')
		  {
		    if(toDecode[counter] == '\0' || 
		       toDecode[counter] == 'e')
		      {
			strcpy(&errBuffer[0], 
			       "Bencoded string ended prematurely!"
			       "Section: Dictionary String Length"
			       " Parsing");
			free(dict);
			free(entry);
			return NULL;
		      }
		    buffer[bufferCounter] = toDecode[counter];
		    bufferCounter++;
		    counter++;
		  }
		stringSize = (size_t) strtol(&buffer[0], NULL, 10);
		buffer[bufferCounter] = ':';
		bufferCounter++;
		counter++;
		for(i = 0; i < stringSize;i++)
		  {
		    if(toDecode[counter] == '\0')
		      {
			strcpy(&errBuffer[0], 
			       "Bencoded string ended prematurely!"
			       "Section: Dictionary String Parsing");
			free(dict);
			free(entry);
			return NULL;
		      }
		    buffer[bufferCounter] = toDecode[counter];
		    bufferCounter++;
		    counter++;
		  }
		buffer[bufferCounter] = '\0';
		obj = decode_bencode(buffer);
		if(dictKeying)
		  {
		    strcpy(entry[size].key , obj->val.string);
		    free_bencodeObject(obj);
		    dictKeying = false;
		    bufferCounter = 0;
		  }
		else
		  {
		    entry[size].val = 
		      (bencodeObject *)					\
		      malloc(sizeof(bencodeObject));
		    entry[size].val->type = obj->type;
		    entry[size].val->val = obj->val;
		    dictKeying = true;
		    size++;
		    bufferCounter = 0;
		  }
	      }
	  }
	dict->size = size;
	dict->first = entry;
	object->val.dict = dict;
	object->type = BC_DCT;
	return object;
      }
      
    default:
      {
	sprintf(&errBuffer[0], "Invalid bencode option: %c", 
		toDecode[counter]);
	return NULL;
      }
    }
}

void free_bencodeObject(bencodeObject *toFree)
{
  size_t i;
  if(toFree->type == BC_LST)
    {
      for(i = 0;i < toFree->listSize;i++)
	{
	  free(toFree->val.list[i]);
	}
    }
  else if(toFree->type == BC_DCT)
    {
      for(i = 0;i < toFree->val.dict->size;i++)
	{
	  free(toFree->val.dict->first[i].key);
	  free(&(toFree->val.dict->first[i]));
	}
      free(toFree->val.dict);
    }
  free(toFree);
}

char *encode_bencode(bencodeObject *toEncode)
{
  //TODO: Implementation
  return NULL;
}

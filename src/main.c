/*
    Copyright (C) 2015 Punkt Creative LLC

    This file is part of Flode.

    Flode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Flode.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdarg.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include "bencode.h"

int
daemon_initalizer()
{
  pid_t pid;
  pid_t sid;
  pid = fork();
  if(pid < 0)
    {
      syslog(LOG_ERR, "floded child process unable to be forked -- exiting...");
      exit(EXIT_FAILURE);
    }
  if(pid > 0)
    {
      exit(EXIT_SUCCESS);
    }
  umask(0);
  sid = setsid();
  if(sid < 0)
    {
      syslog(LOG_ERR, "floded child process unable to obtain proper session ID -- exiting...");
      exit(EXIT_FAILURE);
    }
  if(chdir("/") < 0)
    {
      syslog(LOG_ERR, "floded child process unable to change directories to / -- exiting...");
      exit(EXIT_FAILURE);
    }
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  return 0;
}

int
daemon_config()
{
  uid_t origUid;
  struct stat statbuf;
  int confDes;
  
  origUid = getuid();
  setuid(0);
  //Check to see if we've already got a config directory in /etc/flode
  if(stat("/etc/flode/", &statbuf) != 0 || S_ISDIR(statbuf.st_mode))
  {
    //We don't! Create one.
    if(mkdir("/etc/flode/",S_IRUSR|S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH) == -1)
    {
      syslog(LOG_ERR, "Unable to create /etc/flode/: %m");
      return -1;
    }
    syslog(LOG_INFO, "Created configuration directory /etc/flode/");
  }
  //Do we have a configuration file?
  if(stat("/etc/flode/flode.conf", &statbuf) != 0)
    {
    ssize_t writeLength;
    //Nope!
    confDes = open("/etc/flode/flode.conf", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if(confDes == -1)
      {
	syslog(LOG_ERR, "Unable to create /etc/flode/flode.conf: %m");
	return -1;
      }
    if((writeLength = write(confDes, "//floded configuration file", 27)) == -1)
      {
	syslog(LOG_ERR, "Unable to write to /etc/flode/flode.conf: %m");
	return -1;
      }
    syslog(LOG_INFO, "Created configuration file /etc/flode/flode.conf");
    }
  else
    {
    //Yup!
      confDes = open("/etc/flode/flode.conf", O_RDONLY);
      if(confDes == -1)
	syslog(LOG_ERR, "Unable to open /etc/flode/flode.conf: %m");
    }
  setuid(origUid);
  return 0;
}

int
main(int argc, char* argv[])
{
  bencodeObject *obj;
  size_t i;
  openlog("floded", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_SYSLOG);
  syslog(LOG_INFO, "floded entered");
  daemon_initalizer();
  syslog(LOG_INFO, "floded child process successfully forked");
  syslog(LOG_INFO, "Loading floded configuration files...");
  //daemon_config();
  obj = decode_bencode(BENCODE_TEST_STRING);
  if(obj == NULL)
    {
      syslog(LOG_ERR, "Error: %s", getError());
      return -1;
    }
  for(i = 0; i < obj->val.dict->size;i++)
    {
      syslog(LOG_INFO, "Key: %s", 
	     obj->val.dict->first[i].key);
      syslog(LOG_INFO, "Value: %s", 
	     obj->val.dict->first[i].val->val.string);
    }
  syslog(LOG_INFO, "Size: %d", (int)obj->val.dict->size);
  closelog();
  return(0);
}

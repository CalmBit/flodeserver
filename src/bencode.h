/*
    Copyright (C) 2015 Punkt Creative LLC

    This file is part of Flode.

    Flode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Flode.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef FLODE_BENCODE_H
#define FLODE_BENCODE_H

#include <stdint.h>
#include <stdlib.h>

#define BENCODE_STRING_SIZE 65536

typedef struct bencodeDictEntry bencodeDictEntry;
typedef struct bencodeDict bencodeDict;
typedef struct bencodeObject bencodeObject;

extern char errBuffer[BENCODE_STRING_SIZE];

typedef enum
  {
    BC_STR,
    BC_INT,
    BC_LST,
    BC_DCT
  } bencodeType;

struct bencodeDictEntry
{
  char key[BENCODE_STRING_SIZE];
  bencodeObject *val;
}; 

struct bencodeDict
{
  size_t size;
  bencodeDictEntry *first;
};

struct bencodeObject
{
  bencodeType type;
  size_t listSize;
  union
  {
    int64_t integer;
    char string[BENCODE_STRING_SIZE];
    bencodeObject **list;
    bencodeDict *dict;
  } val;
};

bencodeObject *decode_bencode(char *toDecode);
void free_bencodeObject(bencodeObject* toFree);
char *encode_bencode(bencodeObject *toEncode);
char *getError();

#define BENCODE_TEST_STRING "d4:test5:hello5:test25:world:5:truth18:I Love You Lauren!e"
#endif
